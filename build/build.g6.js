const $utils = require('./build.utils')

console.log('building g6...')

module.exports = {
    outputDir: 'docs/lib',
    transpileDependencies: [
        '@antv',
        'd3-force',
    ],
    configureWebpack: {
        entry: {
            'G6': $utils.resolve('src/build.g6.ts')
        },
        output: {
            filename: `g6.umd.min.js`,
            libraryTarget: 'umd',
            library: ['G6'],
            globalObject: 'this'
        },
    },
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
        config.plugins.delete('copy')
        config.plugins.delete('preload')
        config.plugins.delete('prefetch')

        config.plugins.delete('html')
        config.plugins.delete('hmr')
        config.entryPoints.delete('app')
    }
}