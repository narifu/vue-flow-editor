export const AppData = {
    "nodes": [
        {
            "id": "123456",
            "shape": "activity",
            "x": 590,
            "y": 100,
            "text": "短信抽奖",
            "desc": "发送短信到参与抽奖",
            "img": "/images/activity/advertisement-node.svg"
        },
        {
            "id": "2323456789",
            "shape": "activity",
            "x": 410,
            "y": 405,
            "text": "滚动抽奖",
            "desc": "大屏幕滚动抽奖",
            "img": "/images/activity/interaction-node.svg"
        }
    ],
    "edges": [
        {
            "source": "123456",
            "sourceAnchor": 2,
            "target": "2323456789",
            "targetAnchor": 0
        }
    ]
}