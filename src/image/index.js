import actualSize from './toolbar/actual-size.png'
import copy from './toolbar/copy.png'
import deletePng from './toolbar/delete.png'
import fitView from './toolbar/fit-view.png'
import grid from './toolbar/grid.png'
import preview from './toolbar/pre-view.png'
import redo from './toolbar/redo.png'
import undo from './toolbar/undo.png'
import save from './toolbar/save.png'
import zoomIn from './toolbar/zoomin.png'
import zoomOut from './toolbar/zoomin.png'
import image from './toolbar/image.png'

export const TOOLBAR_IMAGE = {
    actualSize,
    copy,
    delete: deletePng,
    fitView,
    grid,
    preview,
    redo,
    undo,
    save,
    zoomIn,
    zoomOut,
    image,
}
